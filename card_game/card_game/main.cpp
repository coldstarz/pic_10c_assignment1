#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>

using std::cout;
using std::cin;
using std::ofstream;
using std::string;
using std::random_shuffle;
using std::vector;

const int RANK = 10;

void player_draw(double& player, int& count, int deck[], string rank[], string suit[], vector<string>& card, ofstream& output);
void dealer_draw(double& dealer, int& count, int deck[], string rank[], string suit[], vector<string>& card, ofstream& output);


int main(){
	ofstream output;
	output.open("output.txt");

	string rank[] = { "As", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Sota", "Caballo", "Rey" };
	string suit[] = { "Oros", "Bastos", "Espadas", "Copas" }; //arrays of rank and suit

	int deck[40];

	for (size_t i = 0; i < 40; i++){
		deck[i] = i + 1;
	}

	random_shuffle(deck, deck + 40);

	int count = 0;            //count the total number of card drew from the deck.
	double player = 0;
	double dealer = 0;           //total value of the card of players.
	int bet = 0;



	vector<string> player_card;
	vector<string> dealer_card;//vector containing players' cards.

	int player_deposit = 100;   //initial deposit of the player.
	bool player_response = true; //the response of the player.

	while (player_deposit > 0){
		cout << "Place bet: ";
		output << "Place bet";

		cin >> bet;
		output << std::endl << bet << std::endl;

		player_response = true;
		while (player_response){
			string response;

			player_draw(player, count, deck, rank, suit, player_card, output);

			cout << "Your total is: " << player << "\t";
			output << "Your total is: " << player << "\t";

			if (player < 7.5){
				cout << "Do you want another card? y/n ";
				output << "Do you want another card? y/n ";
			}

			cin >> response;
			output << std::endl << response << std::endl;

			if (response == "y")
				player_response = true;
			else
				player_response = false;

			if (dealer < 5.5 && player < 7.5)
				dealer_draw(dealer, count, deck, rank, suit, dealer_card, output);
			cout << "Dealer's total is: " << dealer << "\t" << std::endl << std::endl;
			output << "Dealer's total is: " << dealer << "\t" << std::endl << std::endl;

			if (player_response == false && player_response < 7.5&& dealer >= 5.5 && dealer < 7.5){
				if (player == dealer){
					cout << "Draw!" << std::endl << std::endl;
					output << "Draw" << std::endl << std::endl;
				}
				else if ((7.5 - player) >(7.5 - dealer) && player < 7.5 && dealer < 7.5){
					cout << "You Lose!" << std::endl << std::endl;
					output << "You Lose!" << std::endl << std::endl;
					player_deposit -= bet;
					break;
				}
				else if ((7.5 - player) < (7.5 - dealer) && player < 7.5 && dealer < 7.5){
					cout << "You Win!" << std::endl << std::endl;
					output << "You Win!" << std::endl << std::endl;
					player_deposit += bet;
					break;
				}
				else if (dealer == 7.5){
					cout << "You Lose!" << std::endl << std::endl;
					output << "You Lose!" << std::endl << std::endl;
					player_deposit -= bet;
					break;
				}
				else if (player == 7.5 && dealer < 7.5){
					cout << "You Win!" << std::endl << std::endl;
					output << "You Win!" << std::endl << std::endl;
					player_deposit += bet;
					break;
				}
			}
			else if (player_response == false && dealer < 5.5){
				while (dealer < 5.5){
					dealer_draw(dealer, count, deck, rank, suit, dealer_card, output);
					cout << "Dealer's total is: " << dealer << "\t" << std::endl << std::endl;
					output << "Dealer's total is: " << dealer << "\t" << std::endl << std::endl;
				}

				if (player == dealer){
					cout << "Draw!" << std::endl << std::endl;
					output << "Draw!" << std::endl << std::endl;
				}
				else if ((7.5 - player) > (7.5 - dealer) && player < 7.5 && dealer < 7.5){
					cout << "You Lose!" << std::endl << std::endl;
					output << "You Lose!" << std::endl << std::endl;
					player_deposit -= bet;
					break;
				}
				else if ((7.5 - player) < (7.5 - dealer) && player < 7.5 && dealer < 7.5){
					cout << "You Win!" << std::endl << std::endl;
					output << "You Win!" << std::endl << std::endl;
					player_deposit += bet;
					break;
				}
				else if (dealer == 7.5){
					cout << "You Lose!" << std::endl << std::endl;
					output << "You Lose!" << std::endl << std::endl;
					player_deposit -= bet;
					break;
				}
				else if (player == 7.5 && dealer < 7.5){
					cout << "You Win!" << std::endl << std::endl;
					output << "You Win!" << std::endl << std::endl;
					player_deposit += bet;
					break;
				}
			}
			else if (player > 7.5 || dealer > 7.5){
				if (player > 7.5 && dealer <= 7.5){
					cout << "You Lose!" << std::endl << std::endl;
					output << "You Lose!" << std::endl << std::endl;
					player_deposit -= bet;
					break;
				}
				else if (player <= 7.5 && dealer > 7.5){
					cout << "You Win!" << std::endl << std::endl;
					output << "You Win!" << std::endl << std::endl;
					player_deposit += bet;
					break;
				}
				else if (player > 7.5 && dealer > 7.5){
					cout << "You Lose!" << std::endl << std::endl;
					output << "You Lose!" << std::endl << std::endl;
					player_deposit -= bet;
					break;
				}
			}
		}
	}

	output.close();
	return 0;
}

/*Upon calling this function, the player or the dealer draws a card. And the function keeps 
  track of the cards that they have after this draw and calculate the total value his or her card.*/
void player_draw(double& player, int& count, int deck[], string rank[], string suit[], vector<string>& card, ofstream& output){
	cout << "Your turn:" << std::endl << std::endl;
	output << "Your turn:" << std::endl << std::endl;
	string card_rank = rank[(deck[count] - 1) % RANK];
	string card_suit = suit[(deck[count] - 1) / RANK];   //The rank and suit of the card drawn.
	string card_rank_and_suit = card_rank + " de " + card_suit;
	card.push_back(card_rank_and_suit);

	cout << "New card:" << std::endl;
	output << "New card:" << std::endl;
	cout << "\t\t" << card_rank_and_suit << std::endl << std::endl;
	output << "\t\t" << card_rank_and_suit << std::endl << std::endl;

	cout << "Your card:" << std::endl;
	output << "Your card:" << std::endl;
	for (size_t i = 0; i < card.size(); i++){
		cout << "\t" << "\t" << card[i] << std::endl;
		output << "\t" << "\t" << card[i] << std::endl;
	}

	if ((deck[count] - 1) % RANK < 7)
		player += deck[count] % RANK;
	else
		player = player + 0.5;                                //Add the value of the card to the player's total score. 


	count++;
}

void dealer_draw(double& dealer, int& count, int deck[], string rank[], string suit[], vector<string>& card, ofstream& output){
	cout << "Dealer's turn:" << std::endl << std::endl;
	output << "Dealer's turn:" << std::endl << std::endl;
	string card_rank = rank[(deck[count] - 1 ) % RANK];
	string card_suit = suit[(deck[count] - 1) / RANK];   //The rank and suit of the card drawn.
	string card_rank_and_suit = card_rank + " de " + card_suit;
	card.push_back(card_rank_and_suit);

	cout << "New card:" << std::endl;
	cout << "\t\t" << card_rank_and_suit << std::endl << std::endl;
	output << "New card:" << std::endl;
	output << "\t\t" << card_rank_and_suit << std::endl << std::endl;

	cout << "Dealer's card:" << std::endl;
	output << "Dealer's card:" << std::endl;
	for (size_t i = 0; i < card.size(); i++){
		cout << "\t" << "\t" << card[i] << std::endl;
		output << "\t" << "\t" << card[i] << std::endl;
	}

	cout << std::endl;
	output << std::endl;

	if ((deck[count] - 1) % RANK < 7)
		dealer += deck[count] % RANK;
	else
		dealer = dealer + 0.5;                            //Add the value of the card to the player's total score. 


	count++;
}