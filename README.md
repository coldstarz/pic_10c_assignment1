Filename:     main.cpp

Purpose:      The source code of a console program in which player plays 
              the game of "siete y medio" against the computer.

Assembler:    Tong Wu (uid: 304331735)

Contact:      Tong Wu <wutong0218@g.ucla.edu>.

Mod history:  2016-10-24 Set up the readme.md file.


This source code is my response to the first homework assignment of the
current Pic_10C class that I am taking at UCLA in the Fall quarter of 
2016. The program compiles and runs smoothly but it has some minor 
logical errors that prevents the program from calculating the winning
condition of the game correctly under some circumstances. However, since 
the purpose of the assignment is to let us familiarize with the version 
control system a.k.a GIT, I did not fix the logical error. Also, I did not 
find a classmate who can send a pull request for me because I started 
working on this assignment too late. So I asked one of my friends who are 
familiar with the GIT to do the pull request for me. And I created a new 
Bitbucket account and tried to fork my repository and create a pull request 
by my own to make sure that I know how to create pull requests to other's 
repository.